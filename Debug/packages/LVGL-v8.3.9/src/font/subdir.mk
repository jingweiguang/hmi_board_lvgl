################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../packages/LVGL-v8.3.9/src/font/lv_font.c \
../packages/LVGL-v8.3.9/src/font/lv_font_dejavu_16_persian_hebrew.c \
../packages/LVGL-v8.3.9/src/font/lv_font_fmt_txt.c \
../packages/LVGL-v8.3.9/src/font/lv_font_loader.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_10.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_12.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_12_subpx.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_14.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_16.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_18.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_20.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_22.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_24.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_26.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_28.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_28_compressed.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_30.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_32.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_34.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_36.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_38.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_40.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_42.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_44.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_46.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_48.c \
../packages/LVGL-v8.3.9/src/font/lv_font_montserrat_8.c \
../packages/LVGL-v8.3.9/src/font/lv_font_simsun_16_cjk.c \
../packages/LVGL-v8.3.9/src/font/lv_font_unscii_16.c \
../packages/LVGL-v8.3.9/src/font/lv_font_unscii_8.c 

OBJS += \
./packages/LVGL-v8.3.9/src/font/lv_font.o \
./packages/LVGL-v8.3.9/src/font/lv_font_dejavu_16_persian_hebrew.o \
./packages/LVGL-v8.3.9/src/font/lv_font_fmt_txt.o \
./packages/LVGL-v8.3.9/src/font/lv_font_loader.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_10.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_12.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_12_subpx.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_14.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_16.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_18.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_20.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_22.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_24.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_26.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_28.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_28_compressed.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_30.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_32.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_34.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_36.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_38.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_40.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_42.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_44.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_46.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_48.o \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_8.o \
./packages/LVGL-v8.3.9/src/font/lv_font_simsun_16_cjk.o \
./packages/LVGL-v8.3.9/src/font/lv_font_unscii_16.o \
./packages/LVGL-v8.3.9/src/font/lv_font_unscii_8.o 

C_DEPS += \
./packages/LVGL-v8.3.9/src/font/lv_font.d \
./packages/LVGL-v8.3.9/src/font/lv_font_dejavu_16_persian_hebrew.d \
./packages/LVGL-v8.3.9/src/font/lv_font_fmt_txt.d \
./packages/LVGL-v8.3.9/src/font/lv_font_loader.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_10.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_12.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_12_subpx.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_14.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_16.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_18.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_20.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_22.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_24.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_26.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_28.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_28_compressed.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_30.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_32.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_34.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_36.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_38.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_40.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_42.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_44.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_46.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_48.d \
./packages/LVGL-v8.3.9/src/font/lv_font_montserrat_8.d \
./packages/LVGL-v8.3.9/src/font/lv_font_simsun_16_cjk.d \
./packages/LVGL-v8.3.9/src/font/lv_font_unscii_16.d \
./packages/LVGL-v8.3.9/src/font/lv_font_unscii_8.d 


# Each subdirectory must supply rules for building sources it contributes
packages/LVGL-v8.3.9/src/font/%.o: ../packages/LVGL-v8.3.9/src/font/%.c
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -Os -ffunction-sections -fdata-sections -Wall  -g -gdwarf-2 -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\board\lvgl\demo" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\board\lvgl" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\board\ports\touch\gt911\inc" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\board\ports\wifi" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\board\ports" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\board" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\libraries\HAL_Drivers\config" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\libraries\HAL_Drivers" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\env_support\rt-thread" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\core" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\draw\arm2d" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\draw\nxp\pxp" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\draw\nxp\vglite" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\draw\renesas" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\draw\sdl" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\draw\stm32_dma2d" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\draw\swm341_dma2d" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\draw\sw" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\draw" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\layouts\flex" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\layouts\grid" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\layouts" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\libs\bmp" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\libs\ffmpeg" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\libs\freetype" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\libs\fsdrv" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\libs\gif" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\libs\png" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\libs\qrcode" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\libs\rlottie" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\libs\sjpg" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\libs" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\others\fragment" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\others\gridnav" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\others\ime" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\others\imgfont" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\others\monkey" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\others\msg" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\others\snapshot" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\others" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\themes\basic" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\themes\default" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\themes\mono" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\themes" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\animimg" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\calendar" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\chart" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\colorwheel" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\imgbtn" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\keyboard" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\led" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\list" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\menu" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\meter" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\msgbox" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\span" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\spinbox" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\spinner" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\tabview" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\tileview" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets\win" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra\widgets" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\extra" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\font" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\hal" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\misc" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src\widgets" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\packages\LVGL-v8.3.9\src" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\ra\arm\CMSIS_5\CMSIS\Core\Include" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\ra\fsp\inc\api" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\ra\fsp\inc\instances" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\ra\fsp\inc" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\ra\tes\dave2d\inc" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\ra_cfg\fsp_cfg\bsp" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\ra_cfg\fsp_cfg" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\ra_gen" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\rt-thread\components\drivers\include" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\rt-thread\components\drivers\touch" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\rt-thread\components\finsh" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\rt-thread\components\libc\compilers\common\include" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\rt-thread\components\libc\compilers\newlib" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\rt-thread\components\libc\posix\io\poll" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\rt-thread\components\libc\posix\io\stdio" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\rt-thread\components\libc\posix\ipc" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\rt-thread\include" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\rt-thread\libcpu\arm\common" -I"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\rt-thread\libcpu\arm\cortex-m4" -include"D:\RT-ThreadStudio\workspace\HMI_Board_LVGL\rtconfig_preinc.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

