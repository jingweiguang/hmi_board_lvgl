/**
 * @file lv_demo_music.c
 *
 */

/*********************
 *      INCLUDES
 *********************/


#include "lv_demo_rtt_music.h"
#include "ui/ui.h"

#if LV_USE_DEMO_RTT_MUSIC
void lv_demo_music(void)
{
    ui_init();
}
#endif /*LV_USE_DEMO_RTT_MUSIC*/
