# RA6M3-HMI-Board-lvgl  参考设计说明文档

本设计是在 HMI-Board 开发板上运行的 LVGL 设备健康监控 Demo。基于 RT-Thread 操作系统，支持 RT-Thread Studio/MDK5 软件开发环境。HMI-Board 中控芯片为 RA6M3，支持 2D加速、JPEG 编解码、2M Flash 大容量存储控件，助力用户开发炫酷的图形应用。

本文档包括技术架构、使用说明、资料及文档三部分内容，通过阅读理解本篇内容，可在此参考设计基础上进行二次开发。





